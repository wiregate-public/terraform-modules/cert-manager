variable "acme_server" {
  default = "https://acme-v02.api.letsencrypt.org/directory"
}

variable "cert_manager_chart_version" {
  description = "Cert Manager chart version"
  type        = string
  default     = "1.17.1"
}

locals {
  chart_values = {
    ingressShim = {
      defaultIssuerKind = "ClusterIssuer"
      defaultIssuerName = "cert-manager-default-clusterissuer"
    }
    maxConcurrentChallenges = 300
  }
  cert_manager_default_cluster_issuer = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "cert-manager-default-clusterissuer"
    }
    "spec" = {
      "acme" = {
        "privateKeySecretRef" = {
          "name" = "letsencrypt-key-issuer-prod"
        }
        "server" = "${var.acme_server}"
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "class" = "nginx"
              }
            }
          }
        ]
      }
    }
  }
}
