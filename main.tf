module "dummy_helm_chart" {
  source = "git::https://gitlab.com/wiregate-public/terraform-modules/dummy-helm-chart"
}

data "http" "crds_raw" {
  url = "https://github.com/cert-manager/cert-manager/releases/download/v${var.cert_manager_chart_version}/cert-manager.crds.yaml"
}

resource "terraform_data" "cert_manager_chart_version" {
  input = var.cert_manager_chart_version
}

resource "terraform_data" "crds_raw" {
  input = data.http.crds_raw.response_body
  lifecycle {
    ignore_changes       = [input]
    replace_triggered_by = [resource.terraform_data.cert_manager_chart_version.output]
  }
}

resource "helm_release" "cert_manager_crds" {
  name             = "cert-manager-crds"
  chart            = module.dummy_helm_chart.module_path
  namespace        = "cert-manager"
  create_namespace = true
  depends_on       = [module.dummy_helm_chart]

  set {
    name  = "rawyaml"
    value = base64encode(resource.terraform_data.crds_raw.output)
  }
}

resource "helm_release" "cert_manager_default_clusterissuer" {
  name             = "cert-manager-default-clusterissuer"
  chart            = module.dummy_helm_chart.module_path
  namespace        = "cert-manager"
  create_namespace = true
  depends_on       = [resource.helm_release.cert_manager_crds]

  set {
    name  = "rawyaml"
    value = base64encode(yamlencode(local.cert_manager_default_cluster_issuer))
  }
}

resource "helm_release" "cert_manager" {
  name             = "cert-manager"
  chart            = "cert-manager"
  repository       = "https://charts.jetstack.io"
  version          = var.cert_manager_chart_version
  namespace        = "cert-manager"
  create_namespace = true
  values           = [yamlencode(local.chart_values)]
  depends_on       = [resource.helm_release.cert_manager_crds]
}
