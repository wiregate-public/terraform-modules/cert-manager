# Cert-Manager


## This modules installs:
 - Cert-Manager Custom Resource Definitions (CRDs);
 - Cert-Manager helm chart;
 - Cert-Manager default cluster issuer;

## Variables:
 - cert_manager_chart_version: helm chart version;
 - acme_server: 
       - `https://acme-v02.api.letsencrypt.org/directory`
       - `https://acme-staging-v02.api.letsencrypt.org/directory` for test environment;
 - cert_manager_email;
